import logging
import time
from csv import Dialect, excel
from typing import List, Type

from google.cloud import bigquery
from google.api_core.exceptions import BadRequest

logger = logging.getLogger(__name__)
client = bigquery.Client()
_SF = bigquery.SchemaField


def _build_schema_field(field: str) -> bigquery.SchemaField:
    if "NOTA" in field or "NT" in field:
        return _SF(name=field, field_type="FLOAT", mode="NULLABLE")
    if field.startswith("NU"):
        return _SF(name=field, field_type="INTEGER", mode="NULLABLE")
    return _SF(name=field, field_type="STRING", mode="NULLABLE")


def build_schema(fields: List[str]) -> List[bigquery.SchemaField]:
    return [
        _build_schema_field(field)
        for field in fields
        if field
    ]


def import_raw_data(*gcs_path: str, fields: List[str] = None, suffix: str = "global",
                    dialect: Type[Dialect] = excel):
    config = bigquery.LoadJobConfig()

    config.source_format = "CSV"
    config.field_delimiter = dialect.delimiter
    config.quote_character = dialect.quotechar
    config.null_marker = ""
    config.ignore_unknown_values = True
    config.max_bad_records = 2e5
    config.allow_jagged_rows = True

    config.create_disposition = "CREATE_IF_NEEDED"
    config.write_disposition = "WRITE_TRUNCATE" if suffix != "global" else "WRITE_APPEND"

    if fields:
        config.autodetect = False
        config.skip_leading_rows = 1
        config.schema = build_schema(fields)
    else:
        config.autodetect = True

    dataset: bigquery.Dataset = client.get_dataset("enem_data")
    table: bigquery.TableReference = dataset.table(f"raw_{suffix}")
    if suffix != 'global':
        client.delete_table(table, not_found_ok=True)

    try:
        job: bigquery.LoadJob = client.load_table_from_uri(
            job_id_prefix="enem-data-acquisition-raw-data_",
            source_uris=list(gcs_path),
            location="southamerica-east1",
            destination=table,
            job_config=config,
        )
    except BadRequest as e:
        logger.exception(e)
        return None

    return job


def wait_job(job: bigquery.LoadJob):
    if job is None:
        return
    count = 0
    while not job.done():
        sleep_time = 1 << count
        logger.info(f"Waiting {sleep_time} seconds for load job to complete")
        time.sleep(sleep_time)
        count += 1
