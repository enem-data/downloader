import os


def debug_trees() -> bool:
    return _check_yes("DEBUG_TREES")


def use_local() -> bool:
    return _check_yes("DEBUG_USE_LOCAL")


def iterate() -> bool:
    return _check_no("DEBUG_ITERATE")


def wait_jobs() -> bool:
    return _check_yes("BIGQUERY_WAIT_JOBS")


def initial_year() -> int:
    return int(os.getenv("INITIAL_YEAR", "2009"))


def _check_yes(var: str) -> bool:
    return os.getenv(var, "").lower() in ("yes", "y", "1")


def _check_no(var: str) -> bool:
    return os.getenv(var, "").lower() not in ("no", "n", "0")
