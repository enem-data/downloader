import logging
import subprocess
from pathlib import Path

logger = logging.getLogger(__name__)


def _full_inflate(local: Path):
    for sub in local.iterdir():
        if sub.name.endswith(".zip"):
            # noinspection PyTypeChecker
            subprocess.run(
                args=["unzip", "-o", sub.name],
                cwd=local,
            )
            sub.unlink()
            return sub.resolve().as_posix()
        if sub.name.endswith(".rar"):
            # noinspection PyTypeChecker
            subprocess.run(
                args=["unrar", "x", "-y", sub.name],
                cwd=local,
            )
            sub.unlink()
            return sub.resolve().as_posix()

    for sub in local.iterdir():
        if sub.is_dir():
            full_inflate(sub)


def full_inflate(local: Path):
    while True:
        inflated_file = _full_inflate(local)
        if not inflated_file:
            return
        logger.info(f"Inflated file: {inflated_file}")
