import os
import subprocess
from pathlib import Path
from typing import List

from google.cloud.storage import Client, Bucket, Blob

storage = Client()
bucket_name = f"br_exam_data_{storage.project}"
base_path = Path(f"/{bucket_name}/microdados_enem/")


def get_downloaded_files():
    bucket: Bucket = storage.bucket(bucket_name)

    if not bucket.exists():
        bucket.create(
            location="southamerica-east1",
        )

    prefix = "microdados_enem/"

    blobs: List[Blob] = list(bucket.list_blobs(
        prefix=prefix,
        # delimiter="/",
    ))

    names = (b.name for b in blobs)
    zips = (n for n in names if n.endswith(".zip"))
    suffixes = (n.rsplit('/', 1)[-1] for n in zips)
    years = {int(s.split('.', 1)[0]) for s in suffixes}
    return years


def rsync(local: Path, target_folder: str):
    subprocess.run(
        args=["gsutil", "-m", "rsync", "-r", ".",
              get_path(target_folder)],
        cwd=os.fspath(local),
    )


def lsync(local: Path, target_folder: str):
    subprocess.run(
        args=["gsutil", "-m", "rsync", "-r",
              get_path(target_folder), "."],
        cwd=os.fspath(local),
    )


def get_path(*parts) -> str:
    return f"gs:/{base_path.joinpath(*parts).as_posix()}"
