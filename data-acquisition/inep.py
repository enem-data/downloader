import logging
import os
import re
from collections import namedtuple
from pathlib import Path

import requests
from google.cloud.storage import Bucket, Blob

import config
import gcs
from gcs import storage

logger = logging.getLogger(__name__)
RemoteFile = namedtuple("RemoteFile", ("url", "filename", "year"))
base_path: Path = Path("local_data").resolve()


def get_available_files():
    with requests.get("http://inep.gov.br/microdados") as fl:
        page = fl.text
    ms = re.finditer(
        r"['\"](?P<url>http://download.inep.gov.br/microdados/"
        r"(?P<filename>micro(?:dados)?_enem(?P<year>\d{4}).zip))['\"]",
        page,
    )

    ms = [m.groupdict() for m in ms]

    ms = [RemoteFile(
        url=m['url'],
        filename=m['filename'],
        year=int(m['year'])
    ) for m in ms]
    return ms


def download_file(f: RemoteFile) -> Path:
    logger.info(f"Downloading data from {f.year}")

    folder = base_path.joinpath(str(f.year))
    os.makedirs(folder.resolve().as_posix(), exist_ok=True)
    new_filename = f"{f.year}.zip"

    if config.use_local():
        gcs.lsync(folder, str(f.year))
        return folder

    bucket: Bucket = storage.bucket(gcs.bucket_name)
    blob: Blob = bucket.blob(f"microdados_enem/{f.year}/{new_filename}")

    with requests.get(f.url, stream=True) as req:
        req.raise_for_status()
        content_len = int(req.headers["Content-Length"])
        logger.debug(f"Compressed content size: {content_len / (1 << 20):0.3f} MB")
        blob.upload_from_file(
            file_obj=req.raw,
            content_type=req.headers.get("Content-Type", "application/zip"),
            size=content_len,
        )
        logger.info(f"Completed acquisition of file: {f.filename}")

    local_filepath = folder.joinpath(new_filename)
    logger.debug(f"Local filepath: {local_filepath.resolve().as_posix()}")

    blob.download_to_file(local_filepath.open('wb'))
    logger.info(f"Downloaded from GCS: {new_filename}")
    # if config.debug_trees():
    #     blob.delete()
    return folder
