import logging
import os
import re
import shutil
from pathlib import Path
from typing import Dict, Optional, Union

import config

logger = logging.getLogger(__name__)
Resources = Dict[str, Optional[Path]]


def extract_resources(local: Path) -> Resources:
    year = local.name
    rgxs = {
        'data': re.compile(
            rf".*dados.enem.{year}\.(?:csv|txt)$",
            flags=re.IGNORECASE,
        ),
        'items': re.compile(
            rf".*itens.(?:enem|prove).{year}\.(?:csv|txt)",
            flags=re.IGNORECASE,
        ),
    }
    found: Resources = dict.fromkeys(rgxs.keys())
    all_files = [
        p.relative_to(local)
        for p in local.rglob('*')
        if p.is_file()
    ]
    for p in all_files:
        for t, rgx in rgxs.items():
            if rgx.match(p.as_posix()):
                if not found[t] or p.name.lower().endswith(".csv"):
                    found[t] = p
                    break

    if not found['data']:
        return dict.fromkeys(found)

    return found


def flatten_enem_structure(local: Path) -> Optional[Resources]:
    found = extract_resources(local)

    if not found['data']:
        return found

    if len(found['data'].parts) > 2:
        logger.info("Un-nesting data folder")
        ip: Path = Path(*found['data'].parts[:-2])
        for k, p in found.items():
            found[k] = p.relative_to(ip) if p else None
        ip = local.joinpath(ip)
        for sub in ip.iterdir():
            sub.replace(local.joinpath(sub.relative_to(ip)))
        rmtree(local.joinpath(ip.relative_to(local).parts[0]))

    return found


def rmtree(local: Union[str, bytes, Path, os.PathLike]):
    if config.debug_trees():
        Path(os.fspath(local)).joinpath("~DEBUG_DELETED").touch(exist_ok=True)
    else:
        shutil.rmtree(
            path=os.fspath(local),
            ignore_errors=True,
        )
