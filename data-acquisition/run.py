#!/usr/bin/env python3
import logging
from csv import Sniffer
from pprint import pformat
from typing import List

import bigquery
import config
import decompressor
import gcs
import inep
import organizer

logger = logging.getLogger(__name__)


def get_new_files() -> List[inep.RemoteFile]:
    available = inep.get_available_files()
    downloaded = gcs.get_downloaded_files()

    # Filter only available data since the new standard was implemented
    available = (m for m in available if m.year >= config.initial_year())

    if not config.iterate():
        return [next(available)]

    new_files = [m for m in available if m.year not in downloaded]

    logger.debug(pformat(new_files))
    return new_files


def process_file(f: inep.RemoteFile):
    local = inep.download_file(f)

    decompressor.full_inflate(local)
    resources = organizer.flatten_enem_structure(local)
    gcs.rsync(local, str(f.year))

    if resources['data']:
        with local.joinpath(resources['data']).open('r', encoding='latin-1') as fl:
            head_lines = [fl.readline() for _ in range(5)]
        dialect = Sniffer().sniff(''.join(head_lines))
        headers = [
            header.strip(dialect.quotechar)
            for header in
            head_lines[0].strip().split(dialect.delimiter)
        ]
        del head_lines
        job = bigquery.import_raw_data(
            gcs.get_path(str(f.year), resources['data']),
            fields=headers,
            suffix=str(f.year),
            dialect=dialect,
        )
        if config.wait_jobs():
            bigquery.wait_job(job)

    organizer.rmtree(local)


def download_new():
    new_files = get_new_files()

    for f in new_files:
        process_file(f)


if __name__ == '__main__':
    try:
        import coloredlogs
    except ImportError:
        coloredlogs = None
        logging.basicConfig(level=logging.DEBUG)
    else:
        coloredlogs.install(level=logging.DEBUG)
    download_new()
