#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y python3 python3-pip python python-pip python-crcmod unzip unrar

python3 -m pip install -r requirements.txt
python3 run.py
#gsutil -m rsync -r local_data/ gs://artifacts.$(gcloud config get-value project).appspot.com/microdados_enem/
