import hashlib
import logging
import time

import coloredlogs
import easygoogle
from flask import Request
from googleapiclient.errors import HttpError

logger = logging.getLogger("starter_function")
logging.getLogger("googleapiclient").setLevel(logging.WARNING)
logging.getLogger("urllib3.connectionpool").setLevel(logging.WARNING)
coloredlogs.install(level=logging.DEBUG)

ZONE = "southamerica-east1-c"
MACHINE_TYPE = "n1-highmem-2"
INSTANCE_NAME = "enem-data-downloader"

easygoogle.apply_patch()

service = easygoogle.ServiceAccount.default()
compute = service.get_api("compute", version="v1")

self_hash = hashlib.sha256()
with open(__file__, 'rb') as fl:
    self_hash.update(fl.read())

with open('startup_script.sh') as fl:
    startup_script = fl.read()
    self_hash.update(startup_script.encode())

self_hash = self_hash.hexdigest()


def get_metadata():
    return format_metadata({
        # Startup script is automatically executed by the
        # instance upon startup.
        'startup-script': startup_script,

        # Starter function hash identifier for versioning
        'starter-function-hash': self_hash,
    })


def create_instance():
    image_response = compute.images().getFromFamily(project='ubuntu-os-cloud', family='ubuntu-1804-lts').execute()
    source_disk_image = image_response['selfLink']

    config = {
        'name': INSTANCE_NAME,
        'machineType': f"zones/{ZONE}/machineTypes/{MACHINE_TYPE}",

        # Use Ubuntu 19 image for boot disk
        'disks': [
            {
                'boot': True,
                'autoDelete': True,
                'initializeParams': {
                    'sourceImage': source_disk_image,
                    'diskSizeGb': "60",
                }
            }
        ],

        # Specify a network interface with NAT to access the public
        # internet.
        'networkInterfaces': [{
            'network': 'global/networks/default',
            'accessConfigs': [
                {'type': 'ONE_TO_ONE_NAT', 'name': 'External NAT'}
            ]
        }],

        # Allow the instance to access cloud storage and logging.
        'serviceAccounts': [{
            'email': 'default',
            'scopes': [
                'https://www.googleapis.com/auth/bigquery',
                'https://www.googleapis.com/auth/devstorage.read_write',
                'https://www.googleapis.com/auth/logging.write',
            ]
        }],

        # Metadata is readable from the instance and allows you to
        # pass configuration from deployment scripts to instances.
        'metadata': get_metadata(),
    }

    logger.info("Creating a new instance")
    return compute.instances.insert(
        project=service.projectId,
        zone=ZONE,
        body=config,
    ).execute()


def wait_for_operation(operation):
    while True:
        logger.debug("Waiting for operation to finish")
        result = compute.zoneOperations().get(
            project=service.projectId,
            zone=ZONE,
            operation=operation['name'],
        ).execute()

        if result['status'] == 'DONE':
            print("done.")
            if 'error' in result:
                raise Exception(result['error'])
            return result

        time.sleep(1)


def get_existing_instance():
    logger.info("Getting current instance information")
    try:
        response = compute.instances.get(
            project=service.projectId,
            zone=ZONE,
            instance=INSTANCE_NAME,
        ).execute()
    except HttpError as e:
        if e.resp['status'] == '404':
            logger.warning("There is not instance currently created")
            return None
        raise
    else:
        logger.info("Found instance")
        return response


def update_instance_metadata(fingerprint):
    metadata = get_metadata()
    metadata['fingerprint'] = fingerprint

    logger.info("Updating existing instance metadata")
    return compute.instances().setMetadata(
        project=service.projectId,
        zone=ZONE,
        instance=INSTANCE_NAME,
        body=metadata,
    ).execute()


def delete_instance():
    logger.info("Deleting instance")
    return compute.instances.delete(
        project=service.projectId,
        zone=ZONE,
        instance=INSTANCE_NAME,
    ).execute()


def start_instance():
    logger.info("Booting up instance")
    return compute.instances.start(
        project=service.projectId,
        zone=ZONE,
        instance=INSTANCE_NAME,
    ).execute()


def stop_instance():
    logger.info("Terminating instance")
    return compute.instances.stop(
        project=service.projectId,
        zone=ZONE,
        instance=INSTANCE_NAME,
    ).execute()


def starter_function(request: Request = None):
    _ = request
    existing = get_existing_instance()
    if not existing:
        create_instance()
        return
    meta = parse_metadata(existing.get('metadata', []))
    if meta.get('starter-function-hash') != self_hash:
        if existing['status'] == 'RUNNING':
            operation = stop_instance()
            wait_for_operation(operation)
        operation = update_instance_metadata(existing['metadata']['fingerprint'])
        wait_for_operation(operation)
        start_instance()
    elif existing['status'] == 'RUNNING':
        wait_for_operation(stop_instance())
    wait_for_operation(start_instance())


def parse_metadata(metadata):
    return {
        m['key']: m['value']
        for m in metadata['items']
    }


def format_metadata(metadata):
    return {'items': [
        {'key': k, 'value': v}
        for k, v in metadata.items()
    ]}


if __name__ == '__main__':
    starter_function()
