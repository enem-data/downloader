#!/usr/bin/env bash

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get autoremove --purge -y
echo "Waiting 5 seconds before starting"
sleep 5

if [[ -f install-logging-agent.sh ]]; then
    sudo apt-get install -y --only-upgrade -o Dpkg::Options::="--force-confnew" google-fluentd-catch-all-config
    sudo apt-get install -y --only-upgrade google-fluentd
else
    curl -sSO https://dl.google.com/cloudagents/install-logging-agent.sh
    sudo bash install-logging-agent.sh
fi

if [[ -f install-monitoring-agent.sh ]]; then
    sudo apt-get install -y stackdriver-agent
else
    curl -sSO https://dl.google.com/cloudagents/install-monitoring-agent.sh
    sudo bash install-monitoring-agent.sh
fi


cat > ~/.boto << EOF
[GSUtil]
parallel_composite_upload_threshold = 150M
parallel_composite_upload_component_size = 50M
EOF

cat > /etc/google-fluentd/config.d/syslog.conf << EOF
<source>
  @type tail

  # Parse the timestamp, but still collect the entire line as 'message'
  format /^(?<raw_message>(?<time>[^ ]*\s*[^ ]* [^ ]*) (?<instance_name>[^ ]+) (?<process>[\w\-]+)(?:\[(?<pid>\d+)\])?: (?<severity>DEFAULT|DEBUG|INFO|NOTICE|WARNING|ERROR|CRITICAL|ALERT|EMERGENCY)? ?(?<message>.*))$/

  path /var/log/syslog
  pos_file /var/lib/google-fluentd/pos/syslog.pos
  read_from_head true
  tag syslog
</source>
EOF

sudo service google-fluentd restart

PROJECT_ID=$(gcloud config get-value project)

cd /
rm -rf /data-downloader
mkdir /data-downloader
cd /data-downloader

gsutil cp "gs://br_exam_data_${PROJECT_ID}/microdados_enem/data-acquisition.tar.gz" - | tar xz
echo "$(ls -la)"
cd data-acquisition
./run.sh

shutdown -h +1