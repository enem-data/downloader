#!/usr/bin/env bash

PROJECT_ID=$(cat "${GOOGLE_APPLICATION_CREDENTIALS}" | jq --raw-output '.project_id')
TMP_FILE=$(mktemp)

tar cz data-acquisition > ${TMP_FILE}
gsutil -m cp ${TMP_FILE} "gs://br_exam_data_${PROJECT_ID}/microdados_enem/data-acquisition.tar.gz"
rm ${TMP_FILE}
